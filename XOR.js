var brain = require('brain.js');

const config = {
    binaryThresh: 0.5,
    hiddenLayers: [3],     // array of ints for the sizes of the hidden layers in the network
    activation: 'sigmoid',  // supported activation types: ['sigmoid', 'relu', 'leaky-relu', 'tanh'],
    leakyReluAlpha: 0.01   // supported for activation type 'leaky-relu'
};

var net = new brain.NeuralNetwork(config);

var data = [{input: [0, 0], output: [0]},  
{input: [0, 1], output: [1]},
{input: [1, 0], output: [1]},
{input: [1, 1], output: [0]}];


var config_train = {
                      // Defaults values --> expected validation
iterations: 1000,    // the maximum times to iterate the training data --> number greater than 0
errorThresh: 0.005,   // the acceptable error percentage from training data --> number between 0 and 1
log: true,           // true to use console.log, when a function is supplied it is used --> Either true or a function
logPeriod: 1,        // iterations between logging out --> number greater than 0
learningRate: 0.3,    // scales with delta to effect training rate --> number between 0 and 1
momentum: 0.1,        // scales with next layer's change value --> number between 0 and 1
callback: null,       // a periodic call back that can be triggered while training --> null or function
callbackPeriod: 10,   // the number of iterations through the training data between callback calls --> number greater than 0
timeout: Infinity,
keepNetworkIntact: true    // the max number of milliseconds to train for --> number greater than 0
}

var trainResult = net.train(data,config_train);
var output = net.run([1, 1]);  // [0.933]  
console.log(net)